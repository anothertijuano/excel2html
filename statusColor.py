class StatusColor:
   color="No Status"
   emoji='https://i.imgur.com/QSTk8jc.png'
   background = "#DCDCDE"
   
def readStatusColor(color):
   print(color)
   status=StatusColor()
   if(color==1):
      return(status) 
   rgb=color[-6:]
   r=int(rgb[:2],16)
   g=int(rgb[2:-2],16)
   b=int(rgb[-2:],16)
   if(r>b and g>b):
      status.color="Yellow"
      status.emoji='https://i.imgur.com/3jcwvPe.png'
      status.background="#FFFACA"
   elif(g>r):
      status.color="Green"
      status.emoji='https://i.imgur.com/jXXdsnC.png'
      status.background="#CFFCD2"
   elif(r>g and r>b):
      status.color="Red"
      status.emoji='https://i.imgur.com/OP2pgLU.png'
      status.background="#FFDBE6"
   return(status)

def setCode(cell):
   color=readStatusColor(cell.font.color.index)
   return(color)
   
def setEmoji(cell):
   color=readStatusColor(cell.font.color.index)
   return(color.emoji)

def setColor(cell):
   color=readStatusColor(cell.font.color.index)
   return(color.color)
