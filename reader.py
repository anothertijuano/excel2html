from openpyxl import load_workbook 
from sys import argv
from statusColor import setCode
import re

class Area:
   name=""
   projects=list()

class Project:
   name=""
   customer=""
   country=""
   code=""
   descriptions=list()

def colnum(n):
   string = ""
   while n > 0:
      n, remainder = divmod(n - 1, 26)
      string = chr(65 + remainder) + string
   return string

#Load Execel File
#excelFile=load_workbook(argv[1])
#excelFile=load_workbook('/home/andrade/2.xlsx') 
#get Sheet Names
#names=excelFile.sheetnames

def getFileStructure(fileName):
   excelFile=load_workbook(fileName)
   fileStructure=[]
   for sheetName in excelFile.sheetnames:
      colNames=[]
      sheet=excelFile[sheetName]
      n_cols=len(tuple(sheet.columns))
      if n_cols > 1: 
         try:
            limit=0
            for i in range(1,5):
               cell=sheet[3][n_cols-i].value
               if cell:
                  colNames.append((cell,i))
               else:
                  limit=limit+1
            for i in range(1,10):
               cell=sheet[3][n_cols-limit-i].value
               if cell:
                  colNames.append((cell,i))
               else:
                  limit=limit+1
            fileStructure.append((sheetName,colNames))
         except:
            print("Bad sheet")
   return(fileStructure)


def getAreas(fileName, data):
   excelFile=load_workbook(fileName) 
   areas=[]
   nameCell=3
   customerCell=2
   countryCell=9
   for name, index in data.items():
      areas.append(getStatus(excelFile,name,int(index),nameCell,customerCell,countryCell))
   return(areas)

def getStatus(excelFile,sheetName,column,nameCell,customerCell,countryCell):
   tmpArea=Area()
   tmpArea.name=sheetName
   sheet=excelFile[tmpArea.name]
   n_rows=len(tuple(sheet.rows))
   n_cols=len(tuple(sheet.columns))
   rows=sheet[4:n_rows]
   projects=[]
   for row in rows:
      tmpP=Project()
      tmpP.name=row[nameCell].value
      tmpP.customer=row[customerCell].value
      tmpP.country=row[countryCell].value
      statusText=row[n_cols-column].value
      status=[]
      if(statusText and statusText!="N/A"):
         tmpP.code=setCode(row[n_cols-column])
         for p in re.split('_x000D_|\n',statusText):
            if (p!=' ' and p!='' and p!='\n' and p!='_x000D_'):
               status.append(p)
         tmpP.descriptions=status
         projects.append(tmpP)
   tmpArea.projects=projects
   return(tmpArea)



def projectNI(excelFile):
   tmpA=Area()
   tmpA.name="01 - NI"
   print(tmpA.name)
   sheet=excelFile[tmpA.name]
   n_rows=len(tuple(sheet.rows))
   n_cols=len(tuple(sheet.columns))
   rows=sheet[4:n_rows]
   projects=[]
   for row in rows:
      tmpP=Project()
      tmpP.name=row[3].value
      tmpP.customer=row[2].value
      tmpP.country=row[9].value
      statusText=row[n_cols-1].value
      status=[]
      if(statusText and statusText!="N/A"):
         tmpP.code=setCode(row[n_cols-1])
         for p in statusText.split('\n'):
            if (p!=' ' and p!='' and p!='\n'):
               status.append(p)
         tmpP.descriptions=status
         projects.append(tmpP)
   tmpA.projects=projects
   return(tmpA)

def projectNPI(excelFile):
   tmpA=Area()
   tmpA.name="03 - NPI"
   print(tmpA.name)
   sheet=excelFile[tmpA.name]
   n_rows=len(tuple(sheet.rows))
   n_cols=len(tuple(sheet.columns))
   rows=sheet[4:n_rows]
   projects=[]
   for row in rows:
      tmpP=Project()
      tmpP.name=row[3].value
      tmpP.customer=row[2].value
      tmpP.country=row[9].value
      statusText=row[n_cols-1].value
      status=[]
      if(statusText and statusText!="N/A"):
         tmpP.code=setCode(row[n_cols-1])
         for p in statusText.split('\n'):
            if (p!=' ' and p!='' and p!='\n'):
               status.append(p)
         tmpP.descriptions=status
         projects.append(tmpP)
   tmpA.projects=projects
   return(tmpA)

def projectHomologations(excelFile):
   tmpA=Area()
   tmpA.name="04 - HOMOLOGATIONS"
   print(tmpA.name)
   sheet=excelFile[tmpA.name]
   n_rows=len(tuple(sheet.rows))
   n_cols=len(tuple(sheet.columns))
   print("rows: "+str(n_rows))
   #rows=sheet[4:n_rows-3]
   rows=sheet[4:n_rows]
   projects=[]
   for row in rows:
      tmpP=Project()
      tmpP.name=row[3].value
      tmpP.customer=row[2].value
      tmpP.country=row[9].value
      statusText=row[n_cols-1].value
      status=[]
      if(statusText and statusText!="N/A"):
         tmpP.code=setCode(row[n_cols-1])
         for p in statusText.split('\n'):
            if (p!=' ' and p!='' and p!='\n'):
               status.append(p)
         tmpP.descriptions=status
         projects.append(tmpP)
   tmpA.projects=projects
   return(tmpA)

def projectDay2(excelFile):
   tmpA=Area()
   tmpA.name="02 - DAY 2"
   print(tmpA.name)
   sheet=excelFile[tmpA.name]
   n_rows=len(tuple(sheet.rows))
   n_cols=len(tuple(sheet.columns))
   rows=sheet[4:n_rows]
   projects=[]
   for row in rows:
      tmpP=Project()
      tmpP.name=row[3].value
      tmpP.customer=row[2].value
      tmpP.country=row[9].value
      statusText=row[n_cols-1].value
      status=[]
      if(statusText and statusText!="N/A"):
         tmpP.code=setCode(row[n_cols-1])
         for p in statusText.split('\n'):
            if (p!=' ' and p!='' and p!='\n'):
               status.append(p)
         tmpP.descriptions=status
         projects.append(tmpP)
   tmpA.projects=projects
   return(tmpA)
print("loaded")
