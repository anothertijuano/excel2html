# Excel file to HTML mail report



This is a web app to extract project information from an excel file to an HTML mail report designed with the Outlook Desktop for windows app in mind.

The HTML file structure is defined in reader.py. To preserve the design of the original excel file, a set of functions were defined to read each sheet. Any of those functions can be used as a template for new sheets added.

The information extracted from each sheet is:

- Sheet Name (Area or Team )
- Projects []
  - Project Name
  - Project Health (Based on the color text of the status report)
  - Project Customer
  - Project Country
  - Status report



The excel file can have as many sheets as needed.

HTML templates are located at app/web/templates/  and in order to change the template being used, line 44 of  app/views.py needs to be changed.

A working copy of the system designed specifically for the NOKIA Nuage LAT Team is available on [Heroku](https://excel2htmlreport.herokuapp.com).