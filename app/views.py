# views.py
import sys
from flask import render_template,make_response, request
from reader import getAreas,getFileStructure
from app import app

@app.route('/')
def index():
   resp=make_response(render_template("fileUpload.html"))
   resp.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
   resp.headers["Pragma"] = "no-cache"
   resp.headers["Expires"] = "0"
   return resp

@app.route('/success', methods = ['GET', 'POST'])  
def success():  
   if request.method == 'POST':
      filename='./uploads/excelFile.xlsx'
      f = request.files['file']
      f.save(filename)
      fileStructure=getFileStructure(filename)
      if len(fileStructure) < 1: 
         return render_template("error.html")
      resp=make_response(render_template("selector.html", fileStructure=fileStructure))
      return resp
   return render_template("error.html") 

@app.route('/report', methods = ['GET', 'POST'])  
def report():  
   if request.method == 'POST':
      try: 
         filename='./uploads/excelFile.xlsx'
         result=request.form
         data={}
         selected=[]
         tmpName=''
         for key, value in result.items():
            if not("col" in key):
               selected.append(key)
               tmpName=value
               data[tmpName]='empty'
            else:
               if( key[:-3] in selected):
                  data[tmpName]=value
         print(data)
         areas=getAreas(filename,data)
         return render_template("ReportTemplates/report_style1.html", areas=areas)
      except:
         return render_template("error.html")
   return render_template("error.html") 

@app.errorhandler(500)
def internal_error(error):
   return render_template("error.html") 
