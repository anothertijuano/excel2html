from openpyxl import load_workbook 
from sys import argv

def getSheets(filename):
   excelFile=load_workbook(filename)
   names=excelFile.sheetnames
   return(names)
